$(document).ready(function () {
	$.getJSON("/eazylaunch/js", function (data){
		//init eazylaunch html
		$('body').prepend('<div id="eazylaunch-div"><h2>EazyLaunch</h2><input id="eazylaunch-input"/><div id="eazylaunch-desc"></div></div>');
		$("#eazylaunch-div").hide();
		$("#eazylaunch-desc").html(data.eazylaunch.details);
		
		//handle keydown events
		$(document).keydown(function (e) {
		  if (!e) e = window.event;
		  if (e.keyCode == "Z".charCodeAt(0) && e.ctrlKey) {
			$('#eazylaunch-div').toggle();
			$('#eazylaunch-input').each(function() {
			    $(this).focus();
			    $(this).select();
			});
		    return false;
		  } else if (e.keyCode == 27) { //esc
			  $('#eazylaunch-div').hide();
		  } else return true;
		});
		
		//init eazylaunch autocomplete
		$("#eazylaunch-input").autocomplete(data.eazylaunch.links, {
			formatItem : function(item) {
				return item.title;
			},
			formatMatch : function(item) {
				var title = item.title;
				if (item.root) return '? '+title;
				else return title;
			},
			scrollHeight : 550,
			matchContains : 1,
			minChars : 0,
			max : 25
		}).result(function(event, item) {
			if (item.href.indexOf(Drupal.settings.basePath+"eazylaunch/flush") === 0) //add destination to our flush calls
				item.href += "?destination="+location.pathname.substr(Drupal.settings.basePath.length);
			location.href = item.href;
			$('#eazylaunch-div').hide();
		});
	});
});